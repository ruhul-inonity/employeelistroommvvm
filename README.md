
# EmployeeBuddy CRUD app




TigerIt Technical Interview

Features:

* Employee list page - show employee list (name, age, gender, photo) from local database

* Create a new employee

* Option to update existing employee information

* Delete an existing employee

* Option to export/import employee data
		-- sample data stored at folder named "DB_Exported_Data"
		-- exporting db will create a folder named "EmployeeBuddy" and database dump will be found at path : /storage/emulated/0/EmployeeBuddy/employeeList.json 