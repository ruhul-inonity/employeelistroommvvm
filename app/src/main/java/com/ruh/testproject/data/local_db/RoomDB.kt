package com.ruh.testproject.data.local_db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ruh.testproject.data.local_db.dao.EmployeeDao
import com.ruh.testproject.data.local_db.entity.*

@Database(entities = [Employee::class], version = 1, exportSchema = false)
abstract class RoomDB : RoomDatabase()
{
    abstract fun employeeDao(): EmployeeDao
}