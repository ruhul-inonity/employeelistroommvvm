package com.ruh.testproject.data.network.api_call_factory

import io.reactivex.Maybe
import com.ruh.testproject.data.network.IApiService
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response

class ApiPostCallWithDocument() : IApiCall2 {

    override fun<T> getMaybeObserVable(apiService: IApiService, path: String, hashMap: HashMap<String, T>, MultiPartHashMap: List<MultipartBody.Part>):Maybe<Response<ResponseBody>> {
        return apiService.sendDocuments(path,hashMap as HashMap<String,RequestBody>, MultiPartHashMap)
    }
}