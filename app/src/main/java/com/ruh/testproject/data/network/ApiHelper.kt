package com.ruh.testproject.data.network

import android.location.Location
import com.ruh.testproject.data.network.api_call_factory.ApiDeleteCall
import io.reactivex.Maybe
import com.ruh.testproject.data.network.api_call_factory.ApiGetCall
import com.ruh.testproject.data.network.api_call_factory.ApiPostCall
import com.ruh.testproject.data.network.api_call_factory.ApiPostCallWithDocument
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import sslwireless.android.easy.loyal.merchant.viewmodel.util.ApiCallbackHelper

class ApiHelper(apiService: IApiService) {


    val apiService = apiService

    //call type
    val CALL_TYPE_DELETE = "delete"
    val CALL_TYPE_GET = "get"
    val CALL_TYPE_POST = "post"
    val CALL_TYPE_POST_WITH_DOCUMENT = "post with document"

    //endpoint
    val ENDPOINT_LOGIN = "login"
    val ENDPOINT_LOGOUT = "logout"
    val ENDPOINT_OFFLINE_DROPDOWN = "all-drop-down"
    val ENDPOINT_DASHBOARD = "dashboard"
    val ENDPOINT_TASK_DROPDOWN = "newtask-drop-down"
    val ENDPOINT_NOTIFICATION = "get-notification"
    val ENDPOINT_NOTIFICATION_READ = "notification-mark-as-read"
    val ENDPOINT_COMPANY_LIST = "company-list"
    val ENDPOINT_LOCATION = "location"
    val ENDPOINT_CHECKIN = "attendance"
    val ENDPOINT_CONTACTS = "contacts"
    val ENDPOINT_VERSION = "version"
    val ENDPOINT_CLIENTS = "clients"
    val ENDPOINT_TODOS = "todos"
    val ENDPOINT_COMPANY_DROPDOWN = "company-dropdown"
    val ENDPOINT_FILE_DROPDOWN = "file-title-list"
    val ENDPOINT_LEADS = "new-leads"
    val ENDPOINT_LEADS_FILTER = "new-leads-drop-down"
    val ENDPOINT_ATTENDANCE = "attendance"
    val ENDPOINT_FILES_SAVE = "files-save"
    val TODOS = "todos"
    val VISITLOG = "visitlog"
    val ENDPOINT_CHANGE_PASS = "change-password"
    val ENDPOINT_FORGOT_PASS = "request-forgot-password"


    //api method field key
    val KEY_EMAIL = "email"
    val KEY_PASSWORD = "password"
    val KEY_DEVICE_ID = "device_id"
    val FCM_TOKEN = "fcm_token"
    val KEY_DATE_FROM = "date_from"
    val KEY_DATE_TO = "date_to"

    fun apiLogin(
        msisdn: String,
        password: String,
        deviceId: String,
        fcmToken: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {

        val hashMap = HashMap<String, String>()
        hashMap.put(KEY_EMAIL, msisdn)
        hashMap.put(KEY_PASSWORD, password)
        hashMap.put(KEY_DEVICE_ID, deviceId)
        hashMap.put(FCM_TOKEN, fcmToken)
        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_LOGIN, hashMap)!!.subscribe(apiCallbackHelper)

    }

    fun apiDashBoard(apiCallbackHelper: ApiCallbackHelper) {
        val hashMap = HashMap<String, String>()
        getApiCallObservable(CALL_TYPE_GET, ENDPOINT_DASHBOARD, hashMap)!!.subscribe(
            apiCallbackHelper
        )
    }

    fun apiLeadsList(hashMap: HashMap<String, String>, apiCallbackHelper: ApiCallbackHelper) {
        //val hashMap = HashMap<String, String>()
        getApiCallObservable(CALL_TYPE_GET, ENDPOINT_LEADS, hashMap)!!.subscribe(
            apiCallbackHelper
        )
    }

    fun apiLeadListFilterDetails(apiCallbackHelper: ApiCallbackHelper) {
        val hashMap = HashMap<String, String>()
        getApiCallObservable(CALL_TYPE_GET, ENDPOINT_LEADS_FILTER, hashMap)!!.subscribe(
            apiCallbackHelper
        )
    }

    fun apiSaveLeadUpdate(leadId:String, hashMap: HashMap<String, RequestBody>, file: List<MultipartBody.Part>, apiCallbackHelper: ApiCallbackHelper)
    {
        getApiCallObservableMultipart(CALL_TYPE_POST_WITH_DOCUMENT, "new-leads/${leadId}", hashMap, file).subscribe(apiCallbackHelper)
    }

    /*fun apiSaveLeadUpdate(leadId:String, hashMap: HashMap<String, String>, apiCallbackHelper: ApiCallbackHelper)
    {
        getApiCallObservable(CALL_TYPE_POST, "new-leads/${leadId}", hashMap)!!.subscribe(
            apiCallbackHelper
        )
    }*/


    fun apiTaskDropdown(apiCallbackHelper: ApiCallbackHelper) {
        val hashMap = HashMap<String, String>()
        getApiCallObservable(CALL_TYPE_GET, ENDPOINT_TASK_DROPDOWN, hashMap)!!.subscribe(
            apiCallbackHelper
        )
    }

    fun apiTaskDetails(id: Int, apiCallbackHelper: ApiCallbackHelper) {
        val hashMap = HashMap<String, String>()
        getApiCallObservable(CALL_TYPE_GET, "newtask/${id}", hashMap)!!.subscribe(
            apiCallbackHelper
        )
    }

    fun deleteTask(id: Int, apiCallbackHelper: ApiCallbackHelper) {
        val hashMap = HashMap<String, String>()
        getApiCallObservable(CALL_TYPE_DELETE, "newtask/${id}", hashMap)!!.subscribe(
            apiCallbackHelper
        )
    }

    fun apiTaskStore(
        remarks: String,
        type: String,
        module: String,
        date: String,
        module_id: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        hashMap["remarks"] = remarks
        hashMap["type"] = type
        hashMap["module"] = module
        hashMap["date"] = date
        hashMap["module_id"] = module_id
        getApiCallObservable(CALL_TYPE_POST, "newtask", hashMap)!!.subscribe(apiCallbackHelper)
    }

    fun apiTaskUpdate(
        id: Int,
        remarks: String,
        type: String,
        date: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        hashMap["remarks"] = remarks
        hashMap["type"] = type
        hashMap["date"] = date
        hashMap["_method"] = "put"
        getApiCallObservable(CALL_TYPE_POST, "newtask/${id}", hashMap)!!.subscribe(
            apiCallbackHelper
        )
    }

    fun apiNotificationList(apiCallbackHelper: ApiCallbackHelper) {
        val hashMap = HashMap<String, String>()

        getApiCallObservable(CALL_TYPE_GET, ENDPOINT_NOTIFICATION, hashMap)!!.subscribe(
            apiCallbackHelper
        )
    }

    fun apiGetTaskList(
        pageNo: Int,
        remarks: String,
        isDesc: Boolean,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()

        hashMap.put("remarks", "$remarks")
        hashMap.put("page", "$pageNo")
        if (isDesc) {
            hashMap.put("sorting_type", "desc")
        } else {
            hashMap.put("sorting_type", "asc")
        }
        //newtask? sorting=name&sorting_type=asc&page=1
        getApiCallObservable(CALL_TYPE_GET, "newtask", hashMap)!!.subscribe(apiCallbackHelper)
    }

    fun apiMarkNotificationRead(id: Int, apiCallbackHelper: ApiCallbackHelper) {
        val hashMap = HashMap<String, String>()
        hashMap["id"] = "" + id

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_NOTIFICATION_READ, hashMap)!!.subscribe(
            apiCallbackHelper
        )
    }

    fun apiLogout(apiCallbackHelper: ApiCallbackHelper) {
        val hashMap = HashMap<String, String>()

        getApiCallObservable(CALL_TYPE_GET, ENDPOINT_LOGOUT, hashMap)!!.subscribe(
            apiCallbackHelper
        )
    }

    fun apiOfflineDropDown(apiCallbackHelper: ApiCallbackHelper) {
        val hashMap = HashMap<String, String>()

        getApiCallObservable(CALL_TYPE_GET, ENDPOINT_OFFLINE_DROPDOWN, hashMap)!!.subscribe(
            apiCallbackHelper
        )
    }

    fun apiGetAttendanceDetails(date_from:String, date_to:String, apiCallbackHelper: ApiCallbackHelper) {
        val hashMap = HashMap<String, String>()
        hashMap["date_from"] = date_from
        hashMap["date_to"] = date_to

        getApiCallObservable(CALL_TYPE_GET, "attendance", hashMap)!!.subscribe(
            apiCallbackHelper
        )
    }

    fun apiChangePassword(
        currentPass: String,
        newPass: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        hashMap["current_password"] = currentPass
        hashMap["password"] = newPass
        hashMap["password_confirmation"] = newPass
        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_CHANGE_PASS, hashMap)!!.subscribe(
            apiCallbackHelper
        )
    }

    fun apiForgotPassword(email: String, apiCallbackHelper: ApiCallbackHelper) {
        val hashMap = HashMap<String, String>()
        hashMap["email"] = email
        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_FORGOT_PASS, hashMap)!!.subscribe(
            apiCallbackHelper
        )
    }

    fun apiAttendanceStore(userLoc: Location, apiCallbackHelper: ApiCallbackHelper) {
        val hashMap = HashMap<String, String>()
        hashMap.put("lat", "${userLoc.latitude}")
        hashMap.put("lng", "${userLoc.longitude}")

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_ATTENDANCE, hashMap)!!.subscribe(
            apiCallbackHelper
        )
    }


    fun apiVersionCheck(
        version: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        hashMap.put("version", version)

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_VERSION, hashMap)!!.subscribe(
            apiCallbackHelper
        )
    }

    fun marketScanningList(
        sorting: String,
        sorting_type: String,
        page: String,
        merchant_name: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        hashMap.put("sorting", sorting)
        hashMap.put("sorting_type", sorting_type)
        hashMap.put("page", page)
        hashMap.put("merchant_name", merchant_name)

        getApiCallObservable(CALL_TYPE_GET, "market-scanning", hashMap)!!.subscribe(
            apiCallbackHelper
        )
    }

    fun leadsDetails(
        id: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()

        getApiCallObservable(CALL_TYPE_GET, "new-leads/${id}", hashMap)!!.subscribe(
            apiCallbackHelper
        )
    }

    fun marketScanningDropdownData(
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()

        getApiCallObservable(CALL_TYPE_GET, "market-scanning", hashMap)!!.subscribe(
            apiCallbackHelper
        )
    }

    fun <T> getApiCallObservable(
        callType: String,
        path: String,
        hashMap: HashMap<String, T>
    ): Maybe<Response<ResponseBody>> {
        if (callType.equals(CALL_TYPE_GET)) {
            return ApiGetCall().getMaybeObserVable(apiService, path, hashMap!!)
        } else if (callType.equals(CALL_TYPE_POST)) {
            return ApiPostCall().getMaybeObserVable(apiService, path, hashMap!!)
        } else{ // if (callType.equals(CALL_TYPE_DELETE))
            return ApiDeleteCall().getMaybeObserVable(apiService, path, hashMap!!)
        }
    }

    fun <T> getApiCallObservableMultipart(callType: String, path: String, hashMap: HashMap<String, T>, multiMartHashMap: List<MultipartBody.Part>): Maybe<Response<ResponseBody>>
    {
        return ApiPostCallWithDocument().getMaybeObserVable(apiService, path, hashMap!!, multiMartHashMap)
    }
}
