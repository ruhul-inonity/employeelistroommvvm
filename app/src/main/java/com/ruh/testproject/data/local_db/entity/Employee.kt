package com.ruh.testproject.data.local_db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "employees")
class Employee
{
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @field:SerializedName("id")
    public var id:Int = 0

    @ColumnInfo(name = "employee_name")
    @field:SerializedName("employee_name")
    lateinit var employeeName: String

    @ColumnInfo(name = "gender")
    @field:SerializedName("gender")
    lateinit var gender: String

    @ColumnInfo(name = "age")
    @field:SerializedName("age")
    public var age: Int = 0

    @ColumnInfo(name = "image")
    @field:SerializedName("image")
    lateinit var image: String
}