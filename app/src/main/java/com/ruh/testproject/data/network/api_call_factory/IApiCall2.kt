package com.ruh.testproject.data.network.api_call_factory

import com.ruh.testproject.data.network.IApiService
import io.reactivex.Maybe
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Response

interface IApiCall2 {
    fun<T> getMaybeObserVable(apiService: IApiService, path: String, hashMap: HashMap<String,T>, MultiPartHashMap: List<MultipartBody.Part>): Maybe<Response<ResponseBody>>
}