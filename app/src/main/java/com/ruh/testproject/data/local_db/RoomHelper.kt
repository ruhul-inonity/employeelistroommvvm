package com.ruh.testproject.data.local_db

import android.content.Context
import androidx.room.Room

class RoomHelper(context: Context)  {

    private var context = context
    private val db = Room.databaseBuilder(context, RoomDB::class.java, "TEST_PROJECT").allowMainThreadQueries().build()

    fun getDatabase():RoomDB{
        return db
    }
}