package com.ruh.testproject.data.local_db.dao

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.ruh.testproject.data.local_db.entity.Employee

@Dao
interface EmployeeDao {
    @Query("SELECT * FROM employees")
    abstract fun getAll(): List<Employee>

    @Query("SELECT * FROM employees where id=:id")
    abstract fun getAllById(id: Int): List<Employee>

    @Insert
    abstract fun insert(employees: List<Employee>): List<Long>

    @Insert
    abstract fun insert(employee: Employee)

    @Update
    abstract fun update(employee: Employee): Int

    @Query("UPDATE employees SET employee_name = :name, age = :age, gender = :gender, image = :image WHERE id =:id")
    abstract fun update(id: Int, name:String, age:Int, gender:String, image:String): Int

    @Query("DELETE FROM employees where id=:id")
    abstract fun delete(id : Int): Int

    @Query("DELETE FROM employees")
    abstract fun deleteAll(): Int

    @Delete
    abstract fun delete(employees: List<Employee>): Int

    @Delete
    abstract fun delete(employee: Employee): Int
}