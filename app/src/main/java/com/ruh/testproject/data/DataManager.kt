package com.ruh.testproject.data

import com.ruh.testproject.data.local_db.RoomHelper
import com.ruh.testproject.data.network.ApiHelper
import com.ruh.testproject.data.prefence.PreferencesHelper
import javax.inject.Inject

class DataManager @Inject constructor(
    preferencesHelper: PreferencesHelper,roomHelper: RoomHelper, apiHelper: ApiHelper
) : IDataManager {

    val mPref = preferencesHelper
    val roomHelper = roomHelper
    val apiHelper = apiHelper

}