package com.ruh.testproject.data.network.api_call_factory

import io.reactivex.Maybe
import com.ruh.testproject.data.network.IApiService
import okhttp3.ResponseBody
import retrofit2.Response

class ApiDeleteCall() : IApiCall {

    override fun<T> getMaybeObserVable(apiService: IApiService, path: String, hashMap: HashMap<String,T>): Maybe<Response<ResponseBody>> {
        return apiService.deleteRequest(path,hashMap as HashMap<String,String>)
    }

}