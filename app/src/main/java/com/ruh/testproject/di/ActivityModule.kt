package com.ruh.testproject.di

import com.ruh.testproject.view.activity.main.EmployeeListActivity
import com.ruh.testproject.view.activity.main.EmployeeDetailsActivity
import com.ruh.testproject.view.activity.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    internal abstract fun employeeListActivity(): EmployeeListActivity

    @ContributesAndroidInjector
    internal abstract fun splashActivity(): SplashActivity

    @ContributesAndroidInjector
    internal abstract fun employeeDetailsActivity(): EmployeeDetailsActivity




}