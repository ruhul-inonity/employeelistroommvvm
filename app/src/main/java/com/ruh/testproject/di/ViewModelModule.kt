package com.ruh.testproject.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ruh.testproject.view.activity.main.EmployeeViewModel
import com.ruh.testproject.view.activity.splash.SplashViewModel
import com.ruh.testproject.view.base.BaseViewmodelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: BaseViewmodelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(EmployeeViewModel::class)
    internal abstract fun postMainViewModel(viewModel: EmployeeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    internal abstract fun SplashViewModel(viewModel: SplashViewModel): ViewModel



}