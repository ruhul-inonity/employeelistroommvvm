package com.ruh.testproject.util

import android.content.Context
import android.net.ConnectivityManager

class CheckInternet {
    companion object{
        fun isNetworkConnected(context: Context): Boolean {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo != null
        }
    }
}
