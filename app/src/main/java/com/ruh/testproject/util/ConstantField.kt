package com.ruh.testproject.util


class ConstantField private constructor(){

    var ACCESS_TOKEN = "token"
    var ACCESS_TITLE = "title"

    var LAST_SUCCESSFULL_CACHE_BUILD_TIMESTAMP = "last_successful_cache_build_timestamp"

    companion object {
        private var constantField = ConstantField()
        fun newInstance(): ConstantField {
            return constantField
        }
    }
}