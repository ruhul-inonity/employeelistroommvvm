package com.ruh.testproject.util

import android.content.Context
import androidx.databinding.ViewDataBinding
import com.ruh.testproject.databinding.EmptyPageBinding
import com.ruh.testproject.view.adapter.IAdapterListener
import com.ruh.testproject.view.base.BaseViewHolder


class EmptyViewHolder (itemView: ViewDataBinding, context: Context) :
    BaseViewHolder(itemView.root) {
    var binding = itemView as EmptyPageBinding

    override fun <T> onBind(position: Int, itemModel: T, listener: IAdapterListener) {

    }
}