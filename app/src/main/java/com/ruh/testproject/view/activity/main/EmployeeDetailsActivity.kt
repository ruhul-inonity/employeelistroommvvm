package com.ruh.testproject.view.activity.main

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RadioButton
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import coil.api.load
import coil.transform.RoundedCornersTransformation
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.ruh.testproject.R
import com.ruh.testproject.data.local_db.entity.Employee
import com.ruh.testproject.databinding.ActivityEmployeeDetailsBinding
import com.ruh.testproject.util.LiveDataResult
import com.ruh.testproject.view.base.BaseActivity
import kotlinx.android.synthetic.main.activity_employee_details.*
import okhttp3.ResponseBody
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.*


class EmployeeDetailsActivity : BaseActivity() {

    lateinit var binding: ActivityEmployeeDetailsBinding
    private lateinit var viewModel: EmployeeViewModel
    private var id = 0;
    private var encodedImage = ""

    var imageUpdate:Boolean = false
    var newImageFilePath:String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_employee_details)

        this.viewModel =
            ViewModelProviders.of(this, viewModelFactory)
                .get(EmployeeViewModel::class.java)
    }

    override fun viewRelatedTask() {
        binding.title.text = getString(R.string.add_employee)

        if (intent.hasExtra("id")) {
            id = intent.getIntExtra("id", 0)
            if (id > 0){
                binding.title.text = getString(R.string.edit_employee)
                binding.delete.visibility = View.VISIBLE
                var employeeData = dataManager.roomHelper.getDatabase().employeeDao().getAllById(id)

                fillViewWithData(employeeData)
            }
        }

        binding.ivProfilePic.setOnClickListener(this)
        binding.submit.setOnClickListener(this)
        binding.genderGroup.setOnClickListener(this)
        binding.delete.setOnClickListener(this)
        binding.back.setOnClickListener(this)
    }


    override fun onClick(v: View?) {
        when (v){
            binding.ivProfilePic -> captureOrPickImage();
            binding.submit -> onSubmitPressed()
            binding.back -> onBackPressed()
            binding.delete -> {
                var flag = dataManager.roomHelper.getDatabase().employeeDao().delete(id)
                if (flag > 0) gotoList()
            }

        }
    }

    private fun fillViewWithData(employeeData: List<Employee>) {

        binding.name.setText(employeeData[0].employeeName)
        binding.age.setText("${employeeData[0].age}")

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            loadImage(employeeData[0].image)
            encodedImage = employeeData[0].image
        }

        when (employeeData[0].gender){
            "Male" -> binding.male.isChecked = true
            "Female" -> binding.female.isChecked = true
            "Other" -> binding.other.isChecked = true
        }
    }


    private fun onSubmitPressed() {
        lateinit var radio : RadioButton
        var gid: Int = genderGroup.checkedRadioButtonId
        if (gid!=-1){
             radio = findViewById(gid)
        }


        when {
            binding.name.text.toString().isEmpty() -> {
                showToast(this, "Enter Name !")
            }
            binding.age.text.toString().isEmpty() -> {
                showToast(this, "Enter Age !")
            }
            gid == -1 -> {
                showToast(this, "Gender not selected!")
            }
            else -> {
                val employee = Employee();
                if (newImageFilePath.isNotEmpty()){
                     encodedImage = encoder(newImageFilePath)
                    if (encodedImage.isNotEmpty())
                        employee.image = encodedImage
                }else{
                    employee.image = encodedImage
                }
                employee.employeeName = binding.name.text.toString()
                employee.age = binding.age.text.toString().toInt()
                employee.gender = radio.text.toString()

                var res = 0
                res = if (id > 0){
                    dataManager.roomHelper.getDatabase().employeeDao().update(id, employee.employeeName, employee.age, employee.gender, encodedImage)
                }else{
                    dataManager.roomHelper.getDatabase().employeeDao().insert(employee)
                    1
                }

                if (res > 0) showToast(this, getString(R.string.successful))
                else showToast(this, "Failed to update!")

                gotoList()
            }
        }
    }

    private fun gotoList() {
            val intent = Intent(this@EmployeeDetailsActivity, EmployeeListActivity::class.java)
            startActivity(intent)
            finishAffinity()

    }

    private fun captureOrPickImage() {
        ImagePicker.with(this@EmployeeDetailsActivity)
            .start{ resultCode, data ->
                if(resultCode == Activity.RESULT_OK)
                {
                    val fileUri = data?.data
                    imageUpdate = true
                    newImageFilePath = fileUri?.path.toString()
                    binding.ivProfilePic.load(File(fileUri?.path))
                    {
                        transformations(RoundedCornersTransformation(16f))
                        placeholder(R.drawable.loading_icon)
                    }
                }
            }
    }



    override fun onSuccess(result: LiveDataResult<Response<ResponseBody>>, key: String) {
    }


    private fun compressBitmap(bitmap: Bitmap, quality: Int): Bitmap {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, quality, stream)
        val byteArray = stream.toByteArray()
        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)
    }

    private fun encoder(filePath: String): String{
        val bytes = File(filePath).readBytes()
        return android.util.Base64.encodeToString(bytes, 0)

    }


    fun loadImage(image : String){
        if (image.isNotEmpty()){
            Glide.with(this)
                .load(android.util.Base64.decode(image,0))
                .into(binding.ivProfilePic);
        }else{
            Glide.with(this)
                .load(getDrawable(R.drawable.employees))
                .into(binding.ivProfilePic);
        }
    }


}
