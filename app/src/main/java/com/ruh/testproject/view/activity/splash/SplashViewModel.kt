package com.ruh.testproject.view.activity.splash

import androidx.lifecycle.LifecycleOwner
import com.ruh.testproject.data.DataManager
import com.ruh.testproject.view.base.BaseViewModel
import sslwireless.android.easy.loyal.merchant.viewmodel.util.ApiCallbackHelper
import javax.inject.Inject

class SplashViewModel@Inject constructor(dataManager: DataManager) : BaseViewModel() {
    val dataManager = dataManager

    fun fetchVersionCheck(version: String, lifecycleOwner: LifecycleOwner) {
        dataManager.apiHelper.apiVersionCheck(version, ApiCallbackHelper(livedata(lifecycleOwner,"version")))

    }



}