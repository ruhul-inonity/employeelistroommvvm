package com.ruh.testproject.view.activity.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.ruh.testproject.data.DataManager

class SplashViewModelFactory(
    private val dataManager: DataManager
): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SplashViewModel::class.java!!)) {
            return SplashViewModel(this.dataManager) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}