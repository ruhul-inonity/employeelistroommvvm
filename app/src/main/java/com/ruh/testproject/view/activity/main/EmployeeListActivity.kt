package com.ruh.testproject.view.activity.main

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.ruh.testproject.MyApp.Companion.context
import com.ruh.testproject.R
import com.ruh.testproject.data.local_db.entity.Employee
import com.ruh.testproject.databinding.ActivityEmployeeListBinding
import com.ruh.testproject.databinding.ItemEmployeeBinding
import com.ruh.testproject.util.EmptyViewHolder
import com.ruh.testproject.util.LiveDataResult
import com.ruh.testproject.view.adapter.IAdapterListener
import com.ruh.testproject.view.base.BaseActivity
import com.ruh.testproject.view.base.BaseRecyclerAdapter
import com.ruh.testproject.view.base.BaseViewHolder
import okhttp3.ResponseBody
import retrofit2.Response
import java.io.BufferedReader
import java.io.File


class EmployeeListActivity : BaseActivity() {

    lateinit var binding: ActivityEmployeeListBinding
    private lateinit var viewModel: EmployeeViewModel
    private var filePath = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_employee_list)

        this.viewModel =
            ViewModelProviders.of(this, viewModelFactory)
                .get(EmployeeViewModel::class.java)
    }

    override fun viewRelatedTask() {
        binding.btnAddEmployee.setOnClickListener(this)
        binding.txtExport.setOnClickListener(this)
        binding.txtImport.setOnClickListener(this)

        showEmployeeList()
        filePath = getDirectory()
    }


    private fun showEmployeeList() {
        var employeeList = dataManager.roomHelper.getDatabase().employeeDao().getAll()
        populateEmployeeRecycler(employeeList)
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnAddEmployee -> gotoDetails(-1)
            binding.txtExport -> writeJSONtoFile(filePath)
            binding.txtImport -> readJSONfromFile(filePath)

        }
    }

    private fun gotoDetails(id: Int) {
        val intent = Intent(this@EmployeeListActivity, EmployeeDetailsActivity::class.java)
        intent.putExtra("id", id)
        startActivity(intent)
    }


    private fun populateEmployeeRecycler(notificationList: List<Employee>) {

        binding.recyclerEmployee.layoutManager = LinearLayoutManager(window.context)
        binding.recyclerEmployee.adapter = BaseRecyclerAdapter(window.context, object :
            IAdapterListener {
            override fun <T> clickListener(position: Int, model: T, view: View) {
                model as Employee
                gotoDetails(model.id)
            }

            override fun getViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
                if (viewType > -1) {
                    return RVIH(
                        DataBindingUtil.inflate(
                            LayoutInflater.from(parent.context)
                            , R.layout.item_employee
                            , parent, false
                        )
                        , window.context
                    )
                } else {
                    return EmptyViewHolder(
                        DataBindingUtil.inflate(
                            LayoutInflater.from(parent.context)
                            , R.layout.empty_page
                            , parent, false
                        )
                        , window.context
                    )
                }
            }

        }, notificationList as ArrayList)
    }


    override fun onSuccess(result: LiveDataResult<Response<ResponseBody>>, key: String) {}


    class RVIH(itemView: ViewDataBinding, context: Context) : BaseViewHolder(itemView.root) {
        var binding = itemView as ItemEmployeeBinding
        override fun <T> onBind(position: Int, model: T, mCallback: IAdapterListener) {
            model as Employee

            binding.name.text = "${model.employeeName}"
            binding.age.text = "${model.age}"
            binding.gender.text = "${model.gender}"
            var image = model.image

            if (image.isNotEmpty()) {
                Glide.with(context)
                    .load(android.util.Base64.decode(image, 0))
                    .into(binding.employeePic)
            } else {
                Glide.with(context)
                    .load(context.getDrawable(R.drawable.employees))
                    .into(binding.employeePic);
            }
            binding.root.setOnClickListener {
                mCallback.clickListener(position, model, binding.root)
            }
        }

    }


    private fun getDirectory(): String {

        filePath = when {
            isExternalStorageAvailable -> {
                val exportDir = File(Environment.getExternalStorageDirectory(), "EmployeeBuddy")
                if (!exportDir.exists())
                    exportDir.mkdirs();
                exportDir.absolutePath + "/employeeList.json"
            }
            else -> {
                val folder = filesDir
                val exportDir = File(folder, "EmployeeBuddy")
                exportDir.mkdir()
                exportDir.absolutePath
                exportDir.absolutePath + "/employeeList.json"
            }
        }

        return filePath
    }

    private fun writeJSONtoFile(filePath: String) {

        val permissions =
            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, permissions, 100)
        }else{
            var gson = Gson()
            val jsonString = gson.toJson(dataManager.roomHelper.getDatabase().employeeDao().getAll())
            val file = File(this.filePath)
            file.writeText(jsonString)
            Log.d("employeeList_path", "................. path : $filePath")
            showToast(this, getString(R.string.exported_to) + filePath)
        }
    }


    private fun readJSONfromFile(filePath: String) {
        if (File(filePath).exists()){
            val bufferedReader: BufferedReader = File(filePath).bufferedReader()
            val inputString = bufferedReader.use { it.readText() }

            val type = object : TypeToken<List<Employee>>() {
            }.type
            inputString.let {
                val baseData =
                    Gson().fromJson<List<Employee>>(
                        inputString,
                        type
                    )

                if (baseData != null && baseData.isNotEmpty()) {
                    var flag1 = dataManager.roomHelper.getDatabase().employeeDao().deleteAll()
                    val flag2 = dataManager.roomHelper.getDatabase().employeeDao().insert(baseData)
                    showEmployeeList()
                    showToast(this, getString(R.string.imported_from) + filePath)
                }
            }
        }else{
            showToast(this, getString(R.string.ceate_dir)+filePath)
        }
    }

    private val isExternalStorageAvailable: Boolean
        get() {
            val extStorageState = Environment.getExternalStorageState()
            return Environment.MEDIA_MOUNTED == extStorageState
        }


}
