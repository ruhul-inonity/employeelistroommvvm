package com.ruh.testproject.view.activity.splash

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.ruh.testproject.R
import com.ruh.testproject.databinding.ActivitySplashBinding
import com.ruh.testproject.util.LiveDataResult
import com.ruh.testproject.view.activity.main.EmployeeListActivity
import com.ruh.testproject.view.base.BaseActivity
import okhttp3.ResponseBody
import retrofit2.Response

class SplashActivity : BaseActivity() {

    lateinit var binding: ActivitySplashBinding
    private var timer: Thread? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)
    }

    override fun viewRelatedTask() {
        threadCaller()
    }
    private fun threadCaller() {
        timer = object : Thread() {
            override fun run() {
                try {
                    Thread.sleep(2000)
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    activityLauncher()
                }
            }
        }
        (timer as Thread).start()
    }

    override fun onClick(v: View?) {}

    override fun onSuccess(result: LiveDataResult<Response<ResponseBody>>, key: String) {}


    fun activityLauncher() {
            val intent = Intent(this@SplashActivity, EmployeeListActivity::class.java)
            startActivity(intent)
            finishAffinity()
    }


}
