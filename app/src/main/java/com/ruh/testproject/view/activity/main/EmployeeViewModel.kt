package com.ruh.testproject.view.activity.main

import com.ruh.testproject.data.DataManager
import com.ruh.testproject.view.base.BaseViewModel
import javax.inject.Inject

class EmployeeViewModel @Inject constructor(dataManager: DataManager) : BaseViewModel() {
    val dataManager = dataManager
}